﻿13)void fibonacci(int n) - Prints the first n fibonacci numbers
// C program to print first n Fibonacci numbers 
#include <stdio.h> 
  
// Function to print first n Fibonacci Numbers 
void printFibonacciNumbers(int n) 
{ 
    int f1 = 0, f2 = 1, i; 
  
    if (n < 1) 
        return; 
  
    for (i = 1; i <= n; i++) 
    { 
        printf("%d ", f2); 
        int next = f1 + f2; 
        f1 = f2; 
        f2 = next; 
    } 
} 
  
// Driver Code 
int main() 
{ 
    printFibonacciNumbers(7); 
    return 0; 
} 

o/p:
1 1 2 3 5 8 13