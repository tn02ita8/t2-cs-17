3>A program to convert temperature in Farenheit(F) to Celcius(C)
#include<stdio.h>
void main()
{
    float celsius,fahrenheit;
    printf("\nEnter temperature in Fahrenheit:");
    scanf("%f",&fahrenheit);
    celsius=(fahrenheit - 32)*5/9;
    printf("\nCelsius = %.3f",celsius); //.3f means correct to 3 decimal places
}

o/p:
Enter temperature in Fahrenheit:100
Celsius = 37.7778
