1> a program to convert weight entered in pounds(lbs) to kilogram(kg)
#include <stdio.h>
  #define POUNDTOKG 0.453592
  int main() {
        float pound, kilogram;
        printf("Enter weight in pound:");
        scanf("%f", &pound);
        kilogram = pound * POUNDTOKG;
        printf("%.2f pound = %.2f KiloGram\n", pound, kilogram);
        return 0;
  }
  
 o/p: 
   Enter weight in pound:2.20462
  2.20 pound = 1.00 KiloGram
 