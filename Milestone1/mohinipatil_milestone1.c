1> a program to convert weight entered in pounds(lbs) to kilogram(kg)
#include <stdio.h>
  #define POUNDTOKG 0.453592
  int main() {
        float pound, kilogram;
        printf("Enter weight in pound:");
        scanf("%f", &pound);
        kilogram = pound * POUNDTOKG;
        printf("%.2f pound = %.2f KiloGram\n", pound, kilogram);
        return 0;
  }
  
 o/p: 
   Enter weight in pound:2.20462
  2.20 pound = 1.00 KiloGram
  
  
2>A program to convert length entered in inches to centimeters(cm)
#include <stdio.h>
#include <stdlib.h>
int main()
{
    float in_cm;
    char  in_inches[4];
    printf("Convert inches: ");
    scanf("%s", &in_inches);
    in_cm = atoi(in_inches)*2.54;
    printf("%.2f cmn", in_cm);
    return(0);
}

o/p:
Convert inches: 12
30.48 cm

3>A program to convert temperature in Farenheit(F) to Celcius(C)
#include<stdio.h>
void main()
{
    float celsius,fahrenheit;
    printf("\nEnter temperature in Fahrenheit:");
    scanf("%f",&fahrenheit);
    celsius=(fahrenheit - 32)*5/9;
    printf("\nCelsius = %.3f",celsius); //.3f means correct to 3 decimal places
}

o/p:
Enter temperature in Fahrenheit:100
Celsius = 37.7778

4> program to calculate the Area of a Circle
#include<stdio.h>
#include<math.h>
main()
{
   float radius, area;
   printf("Enter the radius of circle\n"); 
   scanf("%f",&radius);
   area = M_PI*radius*radius;  
   printf("Area of circle = %.2f\n", area);
   return 0;
}

o/p:
Enter the radius of circle 10
Area of circle = 314

5>A program to calculate the Area of a Rectangle
#include<stdio.h>
main()
{
float l,b,area;
printf("Enter length and breadth");
scanf("%f %f",l,b);
area=l*b;
printf("Area= %.2f"&area);
return 0;
getch();
}
o/p:
Enter length and breadth 5 6
Area= 30

6>A program to calculate the Area of a Triangle
#include<stdio.h>
main()
{
float b,h,area;
printf("Enter base and height");
scanf("%f %f",b,h);
area=0.5*b*h;
printf("Area= %.2f"&area);
return 0;
getch();
}
o/p:
Enter base and height 5 6
Area= 15



