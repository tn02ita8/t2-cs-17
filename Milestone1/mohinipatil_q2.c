2>A program to convert length entered in inches to centimeters(cm)
#include <stdio.h>
#include <stdlib.h>
int main()
{
    float in_cm;
    char  in_inches[4];
    printf("Convert inches: ");
    scanf("%s", &in_inches);
    in_cm = atoi(in_inches)*2.54;
    printf("%.2f cmn", in_cm);
    return(0);
}

o/p:
Convert inches: 12
30.48 cm
